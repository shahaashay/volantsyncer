// VolantSyncer.js
// --------------
//     (c) 2018-present Aashay Shah, http://gitlab.com/aashayshah/volantsyncer
//     VolantSyncer may be freely distributed under the MIT license.

var _ = require('lodash');
var { performance }  = require('perf_hooks');

var perform_import = async function(destination_db_knex,destination_table,data_to_import) {
    
	var imported = await destination_db_knex.batchInsert(destination_table,data_to_import);
	return 0;
};

var perform_delete = async function(destination_db_knex,destination_table,key, keys_to_delete) {
	
	if(keys_to_delete.constructor.name == 'Object') {
		console.log('key is a onbje');
		keys_to_delete = keys_to_delete.values();
	}
	
	var deleted = await destination_db_knex(destination_table).whereIn(key, keys_to_delete).del();
	
	return deleted;
};

var ultra_fast_diff = function(origin_key,origin_records, destination_key, destination_records, verbose=true, do_import = true, do_delete = true) {
	if(verbose) console.log('Calculating keys to be imported');
	var origin_map = []; // contains the list of keys for origin records 
	var destination_map = []; // contains the list of keys for dest records
	var elements_to_import = []; // final list of records to be imported
    var elements_to_delete = [];  // final list of records to be deleted
    var is_key_array; // to check if the primary key is a multi column key
    var obj_origin = {}; // objectified version of origin records
	var obj_destination = {}; // objectified version of destination records

    
    if(origin_key.constructor.name == 'Array' && destination_key.constructor.name == 'Array') {
    	is_key_array = true;
    }
	
	if(verbose) console.log('Creating objects of origin records');
	var _t0 = performance.now();
	origin_map = _.map(origin_records, function(object) {
		var key = object[origin_key];
		if(is_key_array) {
			var combined_key = '';
			for(i=0; i < origin_key.length; i++) {

				combined_key += object[origin_key[i]];
				if(i != (origin_key.length - 1)) {
					combined_key += ',';
				}
			}

			key = combined_key;
		}
		
		obj_origin[key]=object;

		return key;
	});

	var _t1 = performance.now();
	if(verbose) console.log('It took (s): ', (_t1-_t0)/1000);
	if(verbose) console.log('Creating a list of destination keys');
	_t0 = performance.now();
	destination_map = _.map(destination_records, function(object) {
		var key = object[destination_key];
		if(is_key_array) {
			var combined_key = '';
			for(i=0; i < destination_key.length; i++) {

				combined_key += object[destination_key[i]];
				if(i != (destination_key.length - 1)) {
					combined_key += ',';
				}
			}

			key = combined_key;
		}
		
		obj_destination[key]=object;

		return key;
	});
	_t1 = performance.now();
	if(verbose) console.log('It took (s): ', (_t1-_t0)/1000);
	
	if(verbose) console.log('Origin keys: ', origin_map.length);
	if(verbose) console.log('Destination keys: ', destination_map.length);
	if(verbose) console.log('Calculating the diff...');

	var _to_import = [];
	var _to_delete = [];

	if(do_import) {
		_t0 = performance.now();

			_to_import = origin_map.filter(function(o) {
			//return !destination_map.includes(o);
			if(obj_destination[o] !== undefined) return false;
			else return true;
		});

		_t1 = performance.now();
		if(verbose) console.log('It took (s): ', (_t1-_t0)/1000);

		if(verbose) console.log('No. of Elements to import:', _to_import.length);

	}

	if(do_delete) {
		_t0 = performance.now();
		var _to_delete = destination_map.filter(function(o){
			//return !origin_map.includes(o);
			if(obj_origin[o] !== undefined) return false;
			else return true;
		});
		_t1 = performance.now();
		if(verbose) console.log('It took (s): ', (_t1-_t0)/1000);

		if(verbose) console.log('No. of Elements to delete:', _to_delete.length);
	}

	var un_map = _.each(_to_import,  function(v) {
		var the_o = obj_origin[v];
		elements_to_import.push(the_o);
	});
	
	if(destination_key.constructor.name=='Array') {
		un_map = _.each(_to_delete,  function(v) {
			let the_o = obj_destination[v];
			let the_array = [];
			for(var i = 0; i<destination_key.length; i++) {
				the_array[i] = the_o[destination_key[i]];
			}
			elements_to_delete.push(the_array);
		});	
	} else {
		un_map = _.each(_to_delete,  function(v) {
			let the_o = obj_destination[v];
			let the_key = the_o[destination_key];
			elements_to_delete.push(the_key);
		});
	}

	
	
	 
	if(verbose) console.log('Finished calculating keys to be imported');
	if(verbose) console.log('Elements to import: ', elements_to_import.length);
	if(verbose) console.log('Elements to delete: ', elements_to_delete.length);

	return {
		elements_to_import: elements_to_import,
		elements_to_delete: elements_to_delete
	}
}

var sync = async function(options) {
	var end_result = {
		origin_elements:0,
		destination_elements:0,
		elements_to_delete:0,
		elements_to_import:0,
		elements_deleted:0,
		elements_imported:0
	};

	var verbose = false;
	var dry_run = false;
	var do_delete = true;
	var do_import = true;
	
	if(options.hasOwnProperty('verbose')) {
		verbose = options.verbose;
	}
	if(options.hasOwnProperty('dry_run')) {
		dry_run = options.dry_run;
	};

	if(options.hasOwnProperty('do_import')) {
		do_import = options.do_import;
	};

	if(options.hasOwnProperty('do_delete')) {
		do_delete = options.do_delete;
	};

	var origin_records = options.origin_records;
	var origin_key = options.origin_key;
	end_result.origin_elements = origin_records.length;
	var destination_records = [];
	var destination_key = options.destination_key;
	var destination_table = options.destination_table;
	var destination_db_handle = options.destination_db_handle;

	let t0 = performance.now();
	destination_records = await destination_db_handle(destination_table).distinct(destination_key);
	let t1 = performance.now();
	end_result.destination_elements = destination_records.length;
	if(verbose) console.log('Fetching destination records took (s): ', (t1-t0)/1000);

	var the_difference = ultra_fast_diff(origin_key, origin_records, destination_key, destination_records, do_import, do_delete);

	end_result.elements_to_import = the_difference.elements_to_import.length;
	end_result.elements_to_delete = the_difference.elements_to_delete.length;

	if(the_difference.elements_to_import.length > 0) {
		console.log('elemenents to import: ', the_difference.elements_to_import.length);
		
		if(!dry_run) var imported = await perform_import(destination_db_handle,destination_table,the_difference.elements_to_import);
		end_result.elements_imported = the_difference.elements_to_import.length
	}

		
	if(the_difference.elements_to_delete.length > 0) {
		if(verbose) console.log('Running delete...');
		if(verbose) console.log('Elements to delete', the_difference.elements_to_delete);
		if(!dry_run)
		var deleted = await perform_delete(destination_db_handle, destination_table, destination_key, the_difference.elements_to_delete);
		end_result.elements_deleted= the_difference.elements_to_delete.length
		if(verbose) console.log('Delete complete.')
	}

	return end_result;
}

var VolantSyncer = {
	sync:sync
};

module.exports = VolantSyncer;