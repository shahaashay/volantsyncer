# VolantSyncer - A tiny utility for PrimaryDB to Secondary DB Syncer

VolantSyncer is based on Knex.js - which is a libaray for building a SQL query for  Postgres, MSSQL, MySQL, MariaDB, SQLite3, Oracle, and Amazon Redshift.

VolantSyncer is made for migrating data from multiple types of databases, say for example MSSQL to MySQL.

VolantSyncer works by adding a schema definition and selecting keys to import. 
You just have provide Volant with Source and Destination DB Handles, the table to sync and mapping of columns.

VolantSyncer checks for DB records to be inserted and DB records to be deleted.

It does not modify the Origin server in anyway. It modifies only the destination DB Server :)

## Installation

```
$ npm install volantsyncer --save
```

### Create a small file called sync_users.js with the following contents
 
```
var knex = require('knex');
var VolantSyncer = require('volantsyncer');

var config = {};

var knex = require('knex');
config.destination_db = knex({
  client: 'mysql2',
  connection: {
    host : '127.0.0.1',
    user : 'username',
    password : 'password',
    database : 'database_name'
  }
});

config.origin_db = knex({
  client: 'mssql',
  connection: {
    host : 'localhost',
    user : 'SA',
    password : 'password',
    database : 'database'
  }
});

var options = {
  origin_db_handle:config.origin_db,
  origin_table:'Tbl_Users',
  origin_key:'User_id', //primary key

  destination_db_handle:config.destination_db,
  destination_table:'users',
  destination_key:'id', // primary key
  selection:[
    'User_id as id',
    'Email as email',
    'Password_hash as password_hash'
  ],
  dry_run:false // set it to true to just get the stats
};

var action = VolantSyncer.run(options);

console.log('Script is running...');

action.then((result)=>{
  console.log('script finished');
  console.table(result);
  process.exit();
});

```

Next exectute it like follows:

```
$ node sync_users.js
```

The result is something like this
```
Script is running...
script finished

┌──────────────────────┬────────┐
│       (index)        │ Values │
├──────────────────────┼────────┤
│   origin_elements    │ 49172  │
│ destination_elements │ 49000  │
│  elements_to_delete  │   4    │
│  elements_to_import  │  176   │
│   elements_deleted   │   4    │
│  elements_imported   │  176   │
└──────────────────────┴────────┘
```

The elements were imported as well as removed to match the elements that were in the origin database.

If you run the script again, you would see:

```
┌──────────────────────┬────────┐
│       (index)        │ Values │
├──────────────────────┼────────┤
│   origin_elements    │ 49172  │
│ destination_elements │ 49172  │
│  elements_to_delete  │   0    │
│  elements_to_import  │   0    │
│   elements_deleted   │   0    │
│  elements_imported   │   0    │
└──────────────────────┴────────┘
```

